const saveButtonPressed = function() {
    console.log("save button clicked");

    // 1. get name and id from the user interface
    let name = document.querySelector("#text-name").value;
    let gpa = document.querySelector("#text-gpa").value;

    // 2. save it to local storage
    localStorage.setItem("studentName", name);
    localStorage.setItem("studentGPA", gpa);
    
    // 3. show confirmation message
    alert("Student saved successfully!");

}
const showStudentsPressed = function() {
    console.log("show button clicked");

    // 1. get the student out of the local storage
    let name = localStorage.getItem("studentName");
    let gpa = localStorage.getItem("studentGPA");

    // 2. display it
    // 2a. get the paragraph
    let p = document.querySelector("#student-results");
    // 2b. update paragraph content with the name and gpa
    p.innerText = "Student Name: " + name + " Student GPA: " + gpa;

}

// click handler for the SAVE button
document.querySelector("#btn-save")
        .addEventListener("click", saveButtonPressed);
// click handler for the SHOW button
document.querySelector("#btn-show")
        .addEventListener("click", showStudentsPressed);
