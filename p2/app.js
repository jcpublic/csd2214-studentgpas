let namesArr = [];
let gpasArr = [];

const saveButtonPressed = function() {
    console.log("save button clicked");

    // 1. get name and id from the user interface
    let name = document.querySelector("#text-name").value;
    let gpa = document.querySelector("#text-gpa").value;

    // 2. add name and gpa to the array
    namesArr.push(name);
    gpasArr.push(gpa);

    // 3. save it to local storage
    // - 1. JSON.strinify
    localStorage.setItem("studentName", JSON.stringify(namesArr));
    localStorage.setItem("studentGPA", JSON.stringify(gpasArr));
    
    // 3. show confirmation message
    alert("Student saved successfully!");

}
const showStudentsPressed = function() {
    console.log("show button clicked");

    // 1. get the name and gpa ARRAY out of the local storage
    let names = JSON.parse(localStorage.getItem("studentName"));
    let gpas = JSON.parse(localStorage.getItem("studentGPA"));

    // 2. display it
    // 2a. get the paragraph
    let p = document.querySelector("#student-results");
    // 2b. update paragraph content with the name and gpa

    for (let i = 0; i < names.length; i++) {
        p.innerText = p.innerText 
                    + "Student Name: " + names[i] + " Student GPA: " + gpas[i]
                    + "\n";
    }
    

}

// click handler for the SAVE button
document.querySelector("#btn-save")
        .addEventListener("click", saveButtonPressed);
// click handler for the SHOW button
document.querySelector("#btn-show")
        .addEventListener("click", showStudentsPressed);
