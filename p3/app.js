let namesArr = [];
let gpasArr = [];

const saveButtonPressed = function() {
    console.log("save button clicked");

    // 1. get name and id from the user interface
    let name = document.querySelector("#text-name").value;
    let gpa = document.querySelector("#text-gpa").value;

    // 2. create a student object
    let student = {
        "name":name,
        "gpa":gpa
    }

    // 3. save it to local storage
    // - 1. JSON.strinify
    localStorage.setItem("student", JSON.stringify(student));
    
    // 3. show confirmation message
    alert("Student saved successfully!");

}
const showStudentsPressed = function() {
    console.log("show button clicked");

    // 1. get the name and gpa ARRAY out of the local storage
    let student = JSON.parse(localStorage.getItem("student"));

    // 2. display it
    // 2a. get the paragraph
    let p = document.querySelector("#student-results");
    // 2b. update paragraph content with the name and gpa

  
    p.innerText = p.innerText 
                + "Student Name: " + student.name 
                + " Student GPA: " + student.gpa;
            

    

}

// click handler for the SAVE button
document.querySelector("#btn-save")
        .addEventListener("click", saveButtonPressed);
// click handler for the SHOW button
document.querySelector("#btn-show")
        .addEventListener("click", showStudentsPressed);
